# Duply

A shell front end for duplicity http://duplicity.nongnu.org/. Duply simplifies the usage by implementing backup job profiles, batch commands and more. 

## Cheat sheet

Run 'duply usage' to get usage help.

First time profile creation:

    duply PROFILE create

Find 2 files in /etc/duply/PROFILE. Edit 'conf':

    SOURCE=/source/dir/
    TARGET='ftp://duply:mypass123@192.168.1.123//duply/my-profile-target'
    GPG_KEY='disabled' # not recommended

Edit 'exclude', this will only backup folder/ but folder/excludeme/ will be excluded:

    - /source/dir/folder/excludeme/
    + /source/dir/folder/
    - **


### USAGE:

    duply PROFILE backup  # backup now
    
    duply PROFILE status # list available backup sets

    duply PROFILE list # list all files in backup

    duply PROFILE purge --force # list and delete outdated backups
    
    duply PROFILE restore /mnt/restore # restore latest backup to /mnt/restore
    
    duply PROFILE fetch etc/passwd /root/pw 4D # restore /etc/passwd from 4 days ago to /root/pw
        see TIME FORMATS in duplicity's manpage (not duply's) 
    
    duply PROFILE backup_verify_purge --force # a one line batch job for cron execution
    
    duply PROFILE pre_full_post # batch job to run a full backup with pre/post scripts

General usage in single or batch mode (see EXAMPLES):  

    duply PROFILE CHAINEDCOMMAND OPTIONS

A chained command has the format

    COMMAND_COMMAND+COMMAND-COMMAND  

For batches the conditional separators can also be written as pseudo commands and(+), or(-). See SEPARATORS for details.

Non duply options are passed on to duplicity (see OPTIONS).
All conf parameters can also be defined in the environment instead.



### PROFILE:

  Indicated by a path or a profile name (PROFILE), which is resolved 
  to '~/.duply/PROFILE' (~ expands to environment variable $HOME).

  Superuser root can place profiles under '/etc/duply'. Simply create
  the folder manually before running duply as superuser.

  Note:  

    Already existing profiles in root's home folder will cease to work
    unless they are moved to the new location manually.

  example 1:   duply myprofile1 backup

  Alternatively a _path_ to a profile might be used which is useful for quick testing, 
  restoring or exotic locations. Shell expansion should work as usual.
  Hint:  

    The path must contain at least one path separator '/', 
    e.g. './test' instead of only 'test'.

  example 2:   duply ~/.duply/myprofile1 backup

### SEPARATORS:

  Multiple commands can be chained using separators. 

    _ (underscore)  
             neutral separator

    + (plus sign), _and_  
             conditional AND
             the next command will only be executed if the previous succeeded

    - (minus sign), _or_  
             conditional OR
             the next command will only be executed if the previous failed

   example:  
    
    'pre+bkp-verify_post' translates to 'pre_and_bkp_or_verify_post'

### COMMANDS:

    version    show duply version (and others like duplicity, python 2, awk grp versions)

    usage      get usage help text

    create     creates a configuration profile
  
    backup     backup with pre/post script execution (batch: pre_bkp_post),
               full (if full_if_older matches or no earlier backup is found)
               incremental (in all other cases)

    bkp        same as 'backup' but without executing pre/post scripts

    pre, post  execute 'PROFILE/pre', 'PROFILE/post' scripts

    full       force full backup

    incr       force incremental backup

    list [<age>]  list all files in backup (as it was at <age>, default: now)

    status     prints backup sets and chains currently in repository

    restore <target_path> [<age>]

               restore the complete backup to <target_path> [as it was at <age>]
   
    fetch <src_path> <target_path> [<age>]  
   
               fetch single file/folder from backup [as it was at <age>]

    and, or     pseudo commands for better batch cmd readability (see SEPARATORS)

    ... (verify, restore, feeeeth, purge, cleanup etc)

### OPTIONS:

    --force    passed to duplicity (see commands: 
               purge, purgeFull, purgeIncr, cleanup)

    --preview  do nothing but print out generated duplicity command lines

    ...

### TIME FORMATS:

  For all time related parameters like age, max_age etc.
  Refer to the duplicity manpage for all available formats. Here some examples:

    2002-01-25T07:00:00+02:00 (full date time format string)

    12D (interval, 12 days ago)

    1h78m (interval, 1 hour 78 minutes ago)

    ...

### PRE/POST SCRIPTS:

  Some useful internal duply variables are exported to the scripts.

    PROFILE, CONFDIR, SOURCE, TARGET_URL_<PROT|HOSTPATH|USER|PASS>, 

    CND_<PREV|NEXT> (condition before/after next/prev command)

    ...

  The CMD_* variables were introduced to allow different actions according to 
  the command the scripts were attached to e.g. 'pre_bkp_post_pre_verify_post' 
  will call the pre script two times, with CMD_NEXT variable set to 'bkp' 
  on the first and to 'verify' on the second run.
  CMD_ERR holds the exit code of the CMD_PREV .

### EXAMPLES:

  create profile 'myprofile1':  

    duply myprofile1 create (don't forget to edit this new conf file)

  backup 'myprofile1' now:  

    duply myprofile1 backup

  list available backup sets of profile 'myprofile1':  

    duply myprofile1 status

  list and delete outdated backups of 'myprofile1':  

    duply myprofile1 purge --force

  restore latest backup of 'myprofile1' to /mnt/restore:  

    duply myprofile1 restore /mnt/restore

  restore /etc/passwd of 'myprofile1' from 4 days ago to /root/pw:  

    duply myprofile1 fetch etc/passwd /root/pw 4D
    (see "duplicity manpage", section TIME FORMATS)

  a one line batch job on 'myprofile1' for cron execution:  

    duply myprofile1 backup_verify_purge --force

  batch job to run a full backup with pre/post scripts:  

    duply myprofile1 pre_full_post

### FILES:

  in profile folder '~/.duply/PROFILE' or '/etc/duply'

  conf             profile configuration file

  pre,post         pre/post scripts (see above for details)

  gpgkey.*.asc     exported GPG key files

  exclude          a globbing list of included or excluded files/folders
                   (see "duplicity manpage", section FILE SELECTION)

### IMPORTANT:

  Copy the *whole* profile folder after the first backup to a safe place.
  It contains everything needed to restore your backups. You will need 
  it if you have to restore the backup from another system (e.g. after a 
  system crash). Keep access to these files restricted as they contain 
  *all* informations (gpg data, ftp data) to access and modify your backups.

  Repeat this step after *all* configuration changes. Some configuration 
  options are crucial for restoration.

## Installatation

See file INSTALL.txt or run `duply usage`

See https://www.heise.de/security/artikel/Hinter-Schloss-und-Siegel-270834.html?seite=all (german)

For multiple versions in parallel see http://duply.net/wiki/index.php/Duply-documentation

Duply needs `duplicity` installed. And `lftp` for ftp storage:

    sudo apt install duplicity lftp


**Be sure to create '/etc/duply' manually for superuser profiles before first invocation.**

    sudo mkdir -p /etc/duply

## Original project: http://duply.net

This is an unofficial mirror. 

Sourceforge: https://sourceforge.net/projects/ftplicity/ 

## Features

- keep recurring settings in profiles per backup job
- check configuration and environment to prevent errors
- one line batch commands supporting conditions (useful in crontab )
  e.g 'backup+verify+purge' translates to 'pre_bkp_post_and_verify_and_purge'
- pre/post scripting for each command via env vars (for e.g. dumping databases)
- automated import/export of keys between profile and keyring

## Licence and copyright

See file LICENCE

(c) 2006 Christiane Rütten

(c) 2008-2018 Edgar Soldin (changes since version 1.3)

## Similar projects

See http://duply.net/#Alternatives

### duplicity-backup.sh

https://github.com/zertrin/duplicity-backup.sh


### Horcrux by Chris Poole

https://github.com/chrispoole643/horcrux/blob/master/horcrux

http://chrispoole.com/project/general/horcrux/